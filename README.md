`const` Tür Niteleyicisi

---

Bu sayfada, C dili tür sistemindeki türler için tanımlanmış niteleyicilerinden (`type qualifiers`: `const` *C89/90*, `volatile` *C89/90*, `restrict` *C99*, `_Atomic` *C11*) olan `const` tür niteleyicisi anlatılmaktadır.

Bazı yerler farklı sebeplerle yanlışlık içeriyor olabilir. Düzenleme yapmaktan çekinmeyin. İletişim için [Telegram](https://t.me/tayyizaman)

Bu yazının gitlab reposu için [tıklayınız](https://gitlab.com/enes1313/c-type-qualifiers-const).

---

<!-- comment *generated with [DocToc](https://github.com/thlorenz/doctoc)* -->

**İçerik**  

- [Tarih](#tarih)
- [Tanım ve Kullanım](#tanım-ve-kullanım)
- [Kısıtlamalar ve Anlamsal Kurallar](#kısıtlamalar-ve-anlamsal-kurallar)
- [Sorunlar](#sorunlar)

## Tarih

C dilinin zaman tüneli *CPL -> BCPL -> B -> K&R C -> ANSI C -> ...* şeklinde olduğu söylenebilir. CPL, BCPL ve B dillerinde `const` kavramı yoktu. CPL dilinde tür kavramı varken, BCPL ve B dillerinde tür kavramı bile yoktu. B dilini geliştiren Dennis Ritchie tür kavramını ekledi. Tür niteleyicileri kavramı ise X3J11 ANSI komitesi tarafından C diline `const` ve `volatile` ile eklendi.

`const` kıvılcımını başlatan kişinin C++ dilini oluşturan Bjarne Stroustrup olması ilginçtir. 1981 yılında Bjarne Stroustrup *C with Classes* için salt okunur/yazılır (`readonly/writeonly`) kavramlarının iyi olacağını düşünür. İlk düşündüğü kavram salt okunura göstericiler (*pointers to readonly*) konseptidir (salt okunur göstericiler (*readonly pointers*) değil). Bu konuyu Dennis Ritchie ile tartışır. Bu tartışma sonrası *Bell Labs C standartları* adı altında kendisinin de dahil olduğu bir grupta önerisini `readonly/writeonly` mekanizmasına dönüşürken bulur. `readonly` isminin `const` olarak yeniden adlandırılmasını söyler ve oradan ayrılır. O gruptaki oylama sonucu C diline `const` niteleyicisi eklenmemiştir. Bjarne Stroustrup, yalnızca global `const` bir nesnenin bir dosyada açık olarak (`èxpilicit`) bildirildiğinde ve yerel (`local`) olduğunda sabitleri temsil etmek için makro kullanımına alternatif olabileceğini düşünmüş. Önerisinin temelinde de bu varmış ancak bilindiği üzerine C dilinde kullanımı böyle olmamıştır (C++ dilinde böyledir). C komitesi de zaten bunun yerine `const` niteleyicisi ile arayüzlerin belirlenmesine odaklanmış. 

X3J11 ANSI komitesinde ortaya tekrar çıkan `const` niteleyicisi C diline eklenmek istenir. X3J11'in tür niteleyicileri ilk başta `const`, `volatile` ve `noalias` niteleyicileridir. Dennis Ritchie ise komiteye, C dilinin kaderini kötüye gitmekten kurtaracak tür niteleyicileri eleştirisini yazar.

Dennis Ritchie tür niteleyicileri kavramına soğuk bakmaktadır. İş işten geçtiğinden dolayı kaldırılmasına yönelik bir talepte bulunmadığını ama tür niteleyicilerinin bir dili tutarsızlaştırdığını söyler. `const` ve `volatile` niteleyicilerinin ağırlıklarını taşımadığını belirtir. Dili öğrenmeye ve kullanmaya yönelik maliyet eleştirisi yapar. `const` için değişiklik yapılması gerektiğini ve `noalias` niteleyecisinin kesinlikle olmaması gerektiğini yazar. Dediği de olur. Komite `noalias` niteleyicisini dile eklememiş ve `const` ile ilgili dediklerinin çoğunu (hepsini değil) uygulamıştır.

`const` ile ilgili tarihi gibi görünen bir konu (`const` atamaları ile alakalı) [Sorunlar](#sorunlar) bölümünde işlenecektir.

## Tanım ve Kullanım

Brian Kernighan ve Dennis Ritchie (K&R) tarafından yazılan Programlama Dili (The C Programing Language) kitabının 2. baskısında (ANSI C standardını açıklar) `const` niteleyicisi ile alakalı geçen tanımları burada vermek güzel olacaktır.

  EN: *The qualifier `const` can be applied to the declaration of any variable to specify that its value will not be changed. For an array, the `const` qualifier says that the elements will not be altered.*

  TR: *`const` niteleyicisi, değerinin değişmeyeceğini belirtmek için herhangi bir değişkenin bildirimine uygulanabilir. Bir dizi için `const` niteleyicisi, dizinin öğelerinin değiştirilmeyeceğini söyler.*

  EN: *... Declaring an object `const` announces that its value will not be changed ...*

  TR: *... Bir nesneyi `const` olarak bildirmek değerinin değişmeyeceğini ilan eder ...*

  EN: *... The purpose of `const` is to announce objects that may be placed in read-only memory, and perhaps to increase opportunities for optimization. ... Except that it should diagnose explicit attempts to change `const` objects, a compiler may ignore these qualifiers. *

  TR: *... `const`'ın amacı salt okunur belleğe yerleştirilebilecek nesneleri duyurmak ve belki de optimizasyon olanaklarını artırmaktır. ... `const` nesnelerini değiştirmeye yönelik açık girişimleri teşhis etmesi dışında, bir derleyici bu niteleyicileri yok sayabilir.*

`const` ile nitelenenin bir nesnenin ismi (`identifier`) değil türü olduğuna dikkat edilmesi gerekir. Tür niteleyici kavramı çoğu programlama dilinde bulunmamaktadır. Yine çoğu programlama dilinde `const` kavramına benzer kullanımlar bir ismin ek özelliği şeklindedir ve nesnenin kendisine yönelik değiştirilemezliğini(`head-const` veya `top-level-const`) sağlamaya çalışırlar. Bunun yanında C dilindeki `const` kavramı bir türün herhangi bir bölümünün `const` olarak ayarlanmasına izin verir, böylece nesnenin kendisinin değişmezliğine (`head-const`), nesnenin bir bölümünün değişmezliğine (`tail-const` veya `low-level-const`) ve bunların bir kombinasyonuna sahip olunabilir.

Kısaca kaynak kodda belirli değişkenlerin doğrudan asla değişmeyeceğinden emin olmak istiyorsak, birilerine değiştirmediğimizi belirtmek istiyorsak, ROM bellekte veri tutmak (derleyici ve ortamı bilerek) istiyorsak `const` tür niteleyicisini kullanırız.

```c

int i1 = 13;
const int i2 = 13;               // i2, const. i2'nin değerini değiştirilmez.
const int * const cpci = &i1;    // cpci ve cpci'nin gösterdiği yer const. cpci ve *cpci değiştirilemez.
const int * f(const int *pci);   // f işlevinin döndürdüğü göstericiyle, göstericinin gösterdiği yer değiştirilemez. f işlevi aldığı pci ile pci'nin gösterdiği değeri değiştirmeyeceğini ima eder.

i2 = 5;                          // hata
cpci = &i2;                      // hata
*cpci = 5;                       // hata

const int *rpci = f(&i1);        // i1 değişmeyecektir (*)
*rpci = 13;                      // hata

// * f fonksiyonu içerisinde *(int *)pci = 13; yapılmadıysa.

```

## Kısıtlamalar ve Anlamsal Kurallar

`const` niteleyicisini içeren sadece *C89/90* standartında bir tane kısıtlama (`constraints`) bulunmaktadır.

EN: The same type qualifier shall not appear more than once in the same specifier list or qualifier list, either directly or via one or more typedef's. (Only in C89)

TR: Bir tür niteleyicisi (`const` veya `volatile`), doğrudan veya bir veya daha fazla typedef aracılığıyla, aynı belirteç veya niteleyici listesinde birden fazla görünmeyecektir. (Sadece C89'da)

İlgili kısıtlama C99 standartıyla kaldırıldı ve birden fazla bildirilen bir tür belirleyicisi bir tane var olacak şekilde anlamsal (`semantic`) kural eklendi.

```c

typedef const int cint_t;
typedef const int *pci_t;
typedef const pci_t cpci_t;
typedef int (*const cpi_t);
typedef const int (*const cpci2_t);
typedef const cint_t ccint_t; /* Sadece C89/90'da hata */

const int a1;
const int * const a2;
const const int e1;           /* Sadece C89/90'da hata */
const int const e2;           /* Sadece C89/90'da hata */
int const const e3;           /* Sadece C89/90'da hata */
const cpi_t e4;               /* Sadece C89/90'da hata */

```

Tüm tür niteleyicileri gibi `const` niteyecisi de yalnızca sol taraf değerli ifadeler (`lvalue expression`) için anlamlıdır. 

`volatile` olmayan `const` nesneleri derleyici tarafından salt okunur (`readonly`) belleğe yerleştirilebilir ve bir `const` nesnesinin adresi bir programda hiç kullanılmıyorsa hiç depolanmayabilir.

C99'dan beri tüm tür niteleyicilerinde olduğu gibi `const` niteleyicisi doğrudan veya bir veya daha fazla typedef aracılığıyla aynı belirteç niteleyici listesinde veya *bildirim belirteçleri (C17 eki)* olarak birden fazla görünüyorsa, davranış yalnızca bir kez görünmüş gibi olacaktır. Yukarıda kısıtlama için verilen örnek C99 standartından beri yasal C kodudur.

`const` nitelikli olmayan türde bir değer kullanılarak `const` nitelikli türle tanımlanan bir nesneyi değiştirme girişiminde bulunmak tanımsız davranıştır (`undefined behavior`).

```c

int *pi;
int e1 = 13;
const int e2 = 13;
const int *pci = &e1;
const char arr[] = "Enes selam iletir!";

*(int *) pci = 1313;       // sorun yok 
*(int *) &e2 = 1313;       // undefined behavior
*strchr(arr, '!') = '.';   // undefined behavior

pci = pi;
pi = pci;       // Hata
pi = (int*)pci; // sorun yok

```

Yine tüm tür niteleyicilerinde olduğu gibi bir dizi türünün bildirimi `const` içeriyorsa, `const` ile C23'e kadar sadece öğe türü; C23 ile hem dizi türü hem öğe türü nitelenir. Bazı derleyiciler C23 standardıyla gelen bu kuralı uyarı veya hata olarak uyguluyorlardı. 

```c

typedef int arr1d_t[2];
typedef int arr2d_t[2][2];

const arr1d_t e1 = {1, 3};
const arr2d_t e2 = {
    {1, 3}, 
    {1, 3}
};

int *p1 = e1;    // hata, e1 : const int * 
void *p2 = &e1;  // C23'e kadar yasal, C23'ten sonra hata
int *p3 = e2[0]; // hata, e2[0] : const int * 
void *p4 = e2;   // C23'e kadar yasal, C23'ten sonra hata

```

Bir işlev (`function`) türünün bildirimi tüm tür niteleyicilerinde olduğu gibi eğer `const` tür niteleyicisiyle bildirilirse, davranış tanımsızdır.

```c

typedef void func_t(void);

int main(void)
{
    const func_t cf;     // undefined behavior
    const func_t *cpf;   // undefined behavior
}

```

C99 standartından beri, bir işlevin dizi türü olan parametresindeki dizi bildiricisinin köşeli parantezleri arasında niteleyiciler varsa ilgili gösterici türü nitelenir.

```c

int e1(const int[13] p);            // int e1(const int * p);
int e2(int[const] p);               // int e2(int * const p);
int e3(int[const const const] p);   // int e3(int * const p);

```

const-nitelikli bileşik değişmezler (`compound literals` *C99*) farklı nesneleri belirtmek zorunda değiller. Diğer bileşik değişmezlerle veya depolanan verilerle aynı veya değerleri örtüşen gösterime sahip olabilirler.

```c

const int* pci1 = (const int[]){1, 2};
const int* pci2 = (const int[]){2, 3};            // pci2, pci1 + 1'e eşit olabilir. 
_Bool b = "foobar" + 3 == (const char[]){"bar"};  // b, true olabilir.

```

Nitelikli iki türün uyumlu olması için, her ikisinin de uyumlu bir türün aynı nitelikli versiyonuna sahip olması gerektiğinden, `const` nitelikli bir tür yalnızca ilgile türle uyumlu bir türün `const` ile niteleneniyle uyumlu olacaktır. Belirteçler veya niteleyiciler listesindeki tür niteleyicilerinin sırası önemli olmadığından `const` için de sıralama önemli değildir.

```c

char *p = 0;
const char **ppcc = &p;  // hata: char* ve const char* uyumlu türler değil. ANSI C standartı bitmeye yakın bu yanlış olan atamaya izin veriliyordu. Sonradan bir uyumsuzluk nedeni ile kaldırıldı. Sorunlar bölümünde işleniyor.
char * const *pcpc = &p;

// Türetilmiş türler için diğer örnekler

int *pi;
const int *pci;
const struct e1_s { int m; } e1 = {1};
struct e1_s e2 = {3};
struct e2_s { int m1; const int m2; } e3 = {1 , 3}, e4 = {1 , 3};

e2 = e1;
pci = &e1.m;
e1 = e2;          // hata
pi = &e1.m;       // hata
e3 = e4;          // hata

```

## Sorunlar

1. Bir `const` engeli

    X3J11 komitesi, C standardını neredeyse bitireceği sırada, C dilinde o anki `const` kullanımı ile bir ihlal bulunur. Bu ihlali düzeltmek için zaman gerekir lakin komitenin zamanı yoktur. Bu nedenle `const` atamaları için bir istisna olarak yinelemeli olan atamalarda her zaman `top-level-const` için atamalar geçerli olacak  olacağı kurallaştırılmıştır. C++ dilinde bu problemi çözmek için C++ komitesinin zamanı varmış ve C++ dilinde mantıklı bir atama kuralı geliştirilmiştir.

    ```c
    
    const char c = 'x';
    char *p1;
    const char **p2 = &p1;   // İlk başta geçerli olan bu atama kaldırıldı. 
    *p2 = &c;
    *p1 = 'X';

    // C dili, p1'in adresini char ** ve char * const *  türlerine atamayı kabul eder. 
    // low-level-const olacak şekilde bir atama kabul edilmez.

    ```

    Aşağıda tüm varyasyonlar ele alınmıştır.

    ```c
    
    int main(void)
    {
        char *p;
        const char *pc;
        char *const cp;
        const char *const cpc;

        char **pp1 = &p;
        char *const *pcp1 = &p;
        char **const cpp1 = &p;
        // const char **ppc1 = &p; hata
        // const char *const *pcpc1 = &p; C++ dilinde geçerli, C'de ne yazık ki geçersiz
        // const char **const cppc1 = &p; hata
        char *const *const cpcp1 = &p;

        // char **pp2 = &pc; hata
        // char *const *pcp2 = &pc; hata
        // char **const cpp2 = &pc; hata
        const char **ppc2 = &pc;
        const char *const *pcpc2 = &pc;
        const char **const cppc2 = &pc;
        // char *const *const cpcp2 = &pc; hata

        // char **pp3 = &cp; hata
        char *const *pcp3 = &cp;
        // char **const cpp3 = &cp; hata
        // const char **ppc3 = &cp; hata
        // const char *const *pcpc3 = &cp; C++ dilinde geçerli, C'de ne yazık ki geçersiz
        // const char **const cppc3 = &cp; hata
        char *const *const cpcp3 = &cp;

        // char **pp4 = &cpc; hata
        // char *const *pcp4 = &cpc; hata
        // char **const cpp4 = &cpc; hata
        // const char **ppc4 = &cpc; hata
        const char *const *pcpc4 = &cpc;
        // const char **const cppc4 = &cpc; hata
        // char *const *const cpcp4 = &cpc; hata
    }

    ```

2. `strchr` problemi

    Dennis Ritche'nin X3J11 komitesine yazdığı eleştiride değindiği problem aslında `const` tür niteleyicisine soğuk bakılan noktalardan biridir. `strchr` ile örneklenen bu problem aslında genel bir problemdir. `strchr` fonksiyonu aldığı karakter dizisini değiştirmeyeceğini taahhüt edeceği için aldığı adres türü `const char *`'dır. Geri dönüş aldığı karakter dizisinin belirli bir elemanının adresi olduğunda programcı için değiştirilebilir bir adres olması kullanışlı olacaktır. İşte tam bu noktada bu fonksiyonun imzasının `char * strchr(char *, int)` mi, yoksa `const char * strchr(const char *, int)` mi olacağı konusunda bir karmaşıklık oluşturur. Bu problem C++ diliyle `template` ile çözülebilen (veya fonksiyon yüklemesi) bir durumken C dilinin kurallarınca `char * strchr(char *, int)` imazasıyla çözülür. Ek olarak bu fonksiyona gönderilen nesne, bu fonksiyonun geri dönüşüyle değişikliğe uğrayacaksa nesnenin kendisinin `const` olmaması programcı sorumluluğunda olmalıdır.

3. C23 ile düzeltilen özellik

    Dizi türlerinin `const` nitelenmesi C23'e kadar öğelerin türlerini nitelemesi derleyicilerin bile uymadığı bir problemdi. Sonuçta dizi türü için gerekli olan tür ismi farklı bir adresi daha tutamayacağı için dizi bildiriminde belirtilen tür niteleyicisinin kendisi için de geçerli olabilmesi sağlanması gerekiyordu.

4. C ile C++ dilindeki temel farklılıklar

    ```c
    
    int gi;
    int gj = 13;
    const int gci;       // C++'da hata. C'de 0.
    const int gcj = 13;  // gci, C++'da iç (internal), C'de dış (external) bağlantıya (linkage) sahip
    const int gck = gj;  // C'de hata.
    const int gck = gcj; // C'de hata.

    void func(void)
    {
        int* p1;
        int i = 5;
        int arr[10];
        const int ci;        // C++'da hata. C'de çöp değer.
        const int cj = 10;
        const int ck = ji;
        const int cl = cj;
        const int *const *pcpci = &p1; // C++'da geçerli. C'de hata
        const int (*p)[10] = &arr; // C++'da geçerli. C'de hata
    }

    ```

----

*Kaynaklar ve Diğer Sayfalar*

[c standards](https://port70.net/~nsz/c/)

[dmr on noalias](https://port70.net/~nsz/c/c89/dmr-on-noalias.html)

[c history](https://www.bell-labs.com/usr/dmr/www/chist.html)

[a damn stupid thing to do the origins of c](https://arstechnica.com/features/2020/12/a-damn-stupid-thing-to-do-the-origins-of-c/)

[wikipedia type qualifier](https://en.wikipedia.org/wiki/Type_qualifier)

[cppreference const](https://en.cppreference.com/w/c/language/const)

[wikipedia const](https://en.wikipedia.org/wiki/Const_%28computer_programming%29)

[usenet const mismatch](http://c-faq.com/ansi/constmismatch.html)

[why const sucks](http://jmdavisprog.com/articles/why-const-sucks.html)

----
